/**
 * Created with IntelliJ IDEA.
 * User: sthangalapally
 * Date: 9/15/13
 * Time: 7:05 PM
 * To change this template use File | Settings | File Templates.
 */
public class Percolation {


    //row/column size of a percolation grid of N*N.
    private int N;

    //which sites are open.
    private boolean[] open;

    //which sites are connected to which other sites. This is exactly what the union-find data structure is designed for.
    private WeightedQuickUnionUF openSites;
    private WeightedQuickUnionUF fullSites;
    private int virtualTop;
    private int virtualBottom;



    public Percolation(int N) {


        if(N<=0){
         throw new IllegalArgumentException();
        }


        this.N = N;
        open = new boolean[N*N];

        for(int i=0; i < open.length; i++){
            open[i] = false;
        }

        openSites = new WeightedQuickUnionUF(N*N+1);
        fullSites = new WeightedQuickUnionUF(N*N+1);
        virtualTop = 0;
        virtualBottom = ufIndex(N, N)+1;

    }

    public boolean isFull(int row, int col) {

            int ufIndex = ufIndex(row, col);

            if (row <= 0 || row > this.N || col <= 0 || col > this.N){
                throw new IndexOutOfBoundsException();
            }

            return (isOpen(row,col) && fullSites.connected(virtualTop,ufIndex));

    }

    public boolean isOpen(int row, int col) {

       int ufIndex = ufIndex(row,col);

        if (row <= 0 || row > this.N || col <= 0 || col > this.N){
            throw new IndexOutOfBoundsException();
        }

        return open[ufIndex];

    }

    public boolean percolates() {
        return openSites.connected(virtualTop, virtualBottom);
    }




    /* The open() method should do three things.
    i.   It should validate the indices of the site that it receives.
    ii.  It should somehow mark the site as open.
    iii. It should perform some sequence of WeightedQuickUnionUF operations that links the site in question to its open neighbors.
     */
    public void open(int i, int j) {

        //i.
        if(validIndices(i,j)){

            if(isOpen(i,j))
                return;

            //ii.

            //mark top row sites as Full as they're connected to virtual top.


            int ufIndex_ij = ufIndex(i,j);
            open[ufIndex_ij] = true;

            if(i==1){
               fullSites.union(virtualTop, ufIndex_ij);
               openSites.union(virtualTop, ufIndex_ij);
            }

            if (i == N)
            {
                openSites.union(virtualBottom,ufIndex_ij);

            }

               //iii.
            if(j<N && isOpen(i, j+1)){
                openSites.union(ufIndex_ij, ufIndex(i, j+1));
                fullSites.union(ufIndex_ij, ufIndex(i, j+1));
            }

            if(j>1 && isOpen(i,j-1)){

                openSites.union(ufIndex_ij, ufIndex(i, j-1));
                fullSites.union(ufIndex_ij, ufIndex(i, j-1));

            }

            if(i>1 && isOpen(i-1,j)){

                openSites.union(ufIndex_ij, ufIndex(i-1, j));
                fullSites.union(ufIndex_ij, ufIndex(i-1, j));
            }

            if(i<N && isOpen(i+1,j)){

                openSites.union(ufIndex_ij, ufIndex(i+1, j));
                fullSites.union(ufIndex_ij, ufIndex(i+1, j));

            }



        }



    }

    //map 2D coordinates to 1D coordinates
    private int ufIndex(int row, int col){

        int ufIndex = 0;

        ufIndex = N * (row -1) + col-1;
            return ufIndex;
      }

    private boolean validIndices(int row, int col){

        if (row <=0 || row > N || col <=0 || col > N)
            throw new IndexOutOfBoundsException("one or both of the indices "+row+", "+col+" are out of bounds");
        else
            return true;

    }


    public static void main(String[] args) {

    }



}
