/**
 * Created by Srinivas on 10/18/2014.
 */
public class PercolationStats {

    private int i, j;

    private double[] results;

    private double const_K95percent = 1.96;

    public PercolationStats(int N, int T){

        results = new double[T];

        if (N <=0 || T <=0){
            throw new IllegalArgumentException("Argument's cannot be less than zero");
        }

        for (int exp_num=0; exp_num < T; exp_num++) {

            int open_sites_per_experiment = 0;

            Percolation perc_grid = new Percolation(N);

            while(!perc_grid.percolates()){

                do {
                    i = StdRandom.uniform(1, N + 1);
                    j = StdRandom.uniform(1, N + 1);
                }while(perc_grid.isOpen(i,j));


                perc_grid.open(i, j);


                open_sites_per_experiment++;
            }

            //x1 equivalent in mue calc from spec.
            results[exp_num] = (double) open_sites_per_experiment / (N*N);

        }
    }

    public double mean(){

        return StdStats.mean(results);

    }

    public double stddev(){

        if (results.length <= 1)
            return Double.NaN;
        return StdStats.stddev(results);
    }

    public double confidenceLo(){

        return mean() - (const_K95percent * stddev()) / Math.sqrt(results.length);
    }

    public double confidenceHi(){

        return mean() + (const_K95percent * stddev()) / Math.sqrt(results.length);
    }

    public static void main(String[] args) {

        int N = StdIn.readInt();
        int T = StdIn.readInt();

        PercolationStats pstats = new PercolationStats(N, T);

        System.out.println("mean                        ="+pstats.mean());
        System.out.println("stdev                       ="+pstats.stddev());
        System.out.println("95% confidence interval     ="+pstats.confidenceLo() +", "+ pstats.confidenceHi());


    }
}
