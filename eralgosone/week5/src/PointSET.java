/**
 * Created with IntelliJ IDEA.
 * User: atypon
 * Date: 9/24/13
 * Time: 4:16 PM
 * To change this template use File | Settings | File Templates.
 */


public class PointSET {


    private SET<Point2D> setofPoints = null;
    private double dist;

           // construct an empty set of points
    public PointSET(){

       this.setofPoints = new SET<Point2D>();

   }

    // is the set empty?
   public boolean isEmpty(){

       return this.setofPoints.isEmpty();
   }

    // number of points in the set
   public int size(){

       return this.setofPoints.size();
   }

    // add the point p to the set (if it is not already in the set)
   public void insert(Point2D p){

       if(!this.setofPoints.contains(p))
           this.setofPoints.add(p);
       else
           System.out.println("Point already exists!");


   }

    // does the set contain the point p?
   public boolean contains(Point2D p){

       return this.setofPoints.contains(p);

   }

    // draw all of the points to standard draw
   public void draw(){

       for (Point2D pointofset : this.setofPoints) {

           pointofset.draw();

       }

  }

    // all points in the set that are inside the rectangle
   public Iterable<Point2D> range(RectHV rect){

       SET<Point2D> insideRectSet = new SET<Point2D>();

       for (Point2D pointofSet : this.setofPoints) {

           if(rect.contains(pointofSet))
           {
                insideRectSet.add(pointofSet);

           }

       }
       return insideRectSet;
   }


   // a nearest neighbor in the set to p; null if set is empty
   public Point2D nearest(Point2D p){

       double minDist = Integer.MAX_VALUE;
       Point2D nearNeighbour = null;


       for (Point2D pointofSet : this.setofPoints) {

           dist = pointofSet.distanceSquaredTo(p);

           if( dist < minDist){

               minDist = dist;
               nearNeighbour = pointofSet;

           }


       }

       return  nearNeighbour;

   }


    public static void main(String[] args) {


        PointSET pset = new PointSET();
        String lineWithSpaces;
        String[] line;
        int N = 0;

       In file = new In("/home/sthangalapally/workspace/Courses/Analysis-I/Assignments/ideaProjects/eralgosone/week5/data/circle100.txt");

     //   In file = new In("/home/sthangalapally/workspace/Courses/Analysis-I/Assignments/ideaProjects/eralgosone/week5/data/input1M.txt");

        while(!file.isEmpty())
        {
            lineWithSpaces = file.readLine().trim();
            line = lineWithSpaces.split("\\s+");



                Double i = Double.parseDouble(line[0]);
                Double j = Double.parseDouble(line[1]);

                Point2D p = new Point2D(i,j);
                pset.insert(p);

        }
        pset.draw();


    }

    }

