import java.awt.*;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: atypon
 * Date: 9/24/13
 * Time: 4:16 PM
 * To change this template use File | Settings | File Templates.
 */
public class KdTree {

    private Node root = null;
    private int N;
    private static final boolean VERTICAL   = true;
    private static final boolean HORIZONTAL = false;

    private RectHV rect;
    private Node parent;
    private Node node;
    private RectHV nodeRect;

    private ArrayList<Point2D> plist;
    private Point2D nearestNeighborSoFar;
    private Point2D nearestNeighbor;
    private double minDistSoFar = Integer.MAX_VALUE;
    private double currDist;

    private static class Node {
       private Point2D point;      // the point
       private RectHV rect;    // the axis-aligned rectangle corresponding to this node
       private Node lb;        // the left/bottom subtree
       private Node rt;        // the right/top subtree
       private boolean orientation;
       private Node parent;

        public Node(Point2D p, boolean orient, Node parent, RectHV rect) {

            this.point = p;
            this.orientation =  orient;
            this.parent = parent;
            this.rect = rect;

        }

    }




    public boolean isEmpty() {
        return size() == 0;
    }

    public int size()
    {
        return size(root);
    }

    private int size(Node x)
    {

        if (x == null) return 0;

        else{

         N = size(x.lb)+size(x.rt) + 1;

        }

         return N;

    }

    public boolean contains(Point2D p) {
        return get(p) != null;
    }

    // return Point2D point, or null if no such point exists
    public Point2D get(Point2D p) {
        return get(root, p);
    }

    private Point2D get(Node x, Point2D p) {

        if (x == null){ return null;
        }

        else if(x.orientation == HORIZONTAL){

                    int cmp = p.compareTo(x.point);
                    if      (cmp < 0){
                         return get(x.lb,  p);
                    }
                    else if (cmp > 0){
                         return get(x.rt, p);
                    }
                    else if(cmp == 0)
                        return x.point;
        }
        else if ( x.orientation == VERTICAL){

                      int  cmp = cmp = p.compareTo(x.point);
                        if      (cmp < 0){
                           return get(x.lb,  p);
                        }
                        else if (cmp > 0){
                           return get(x.rt, p);
                        }
                        else if(cmp ==0) return x.point;
            }


        return null;
    }


    public void put(Point2D p) {

        root = put(root, p, VERTICAL, parent, rect);

    }

    private Node put(Node x, Point2D p, boolean orient, Node parent, RectHV rect) {

        if(x == root){
            rect = new RectHV(0,0,1,1);
        }

        if (x == null) x = new Node(p, orient, parent, rect);

        else{

            if(orient == HORIZONTAL){

            int cmp = Point2D.Y_ORDER.compare(p, x.point);

            parent = x;

                if      (cmp < 0){

                    rect = setAxisAlignedRect(cmp, orient,parent);

                    x.lb  = put(x.lb,  p, VERTICAL, parent, rect);


                }
                else if (cmp > 0){

                    rect = setAxisAlignedRect(cmp, orient,parent);

                    x.rt = put(x.rt, p, VERTICAL, parent, rect);

                }

            }
            else if ( orient == VERTICAL){

                int cmp = Point2D.X_ORDER.compare(p, x.point);

                parent = x;

                if      (cmp < 0){

                    rect = setAxisAlignedRect(cmp, orient,parent);

                    x.lb  = put(x.lb,  p, HORIZONTAL, parent, rect);

                }
                else if (cmp > 0){

                    rect = setAxisAlignedRect(cmp, orient,parent);

                    x.rt = put(x.rt, p, HORIZONTAL, parent, rect);

                }

            }
        }




            return x;

        }

    private RectHV setAxisAlignedRect(int cmp, boolean orient, Node parent) {

        if(orient == HORIZONTAL){

            if(cmp < 0){

                double xmin = parent.rect.xmin();
                double ymin = parent.rect.ymin();
                double xmax = parent.rect.xmax();
                double ymax = parent.point.y();

                rect = new RectHV(xmin,ymin,xmax, ymax);


            }else if(cmp > 0){

                double xmin = parent.rect.xmin();
                double ymin = parent.point.y();
                double xmax = parent.rect.xmax();
                double ymax = parent.rect.ymax();

                rect = new RectHV(xmin,ymin,xmax,ymax);


            }
        }
            else if (orient == VERTICAL){

                if(cmp < 0){

                    double xmin = parent.rect.xmin();
                    double ymin = parent.rect.ymin();
                    double xmax = parent.point.x();
                    double ymax = parent.rect.ymax();

                    rect = new RectHV(xmin,ymin,xmax, ymax);



                }else if(cmp > 0){

                    double xmin = parent.point.x();
                    double ymin = parent.rect.ymin();
                    double xmax = parent.rect.xmax();
                    double ymax = parent.rect.ymax();

                    rect = new RectHV(xmin,ymin,xmax, ymax);



                }




            }




        return rect;

    }


    public void insert(Point2D p) {

        put(p);

      }


    /*
      To find a closest point to a given query point, start at the root and recursively search in both subtrees
      using the following pruning rule: if the closest point discovered so far is closer than the distance between
      the query point and the rectangle corresponding to a node, there is no need to explore that node (or its subtrees)
      a node is searched only if it might contain a point that is closer than the best one found so far.

        */

    public Point2D nearest(Point2D query) {


        nearestNeighborSoFar = nearest(root, query);

        return nearestNeighborSoFar;

}

    private Point2D nearest(Node x, Point2D query) {


        if (x == null){ return nearestNeighborSoFar;
        }
        else{

            currDist = query.distanceTo(x.point);
            if( currDist < minDistSoFar ){

                minDistSoFar = currDist;
                nearestNeighborSoFar = x.point;

            }

        }

        if(x.orientation == HORIZONTAL){

            int cmp = Point2D.Y_ORDER.compare(query, x.point);
            if      (cmp < 0){
                nearestNeighborSoFar = nearest(x.lb, query);
            }
            else if (cmp > 0){
                nearestNeighborSoFar = nearest(x.rt, query);
            }

        }
        else if ( x.orientation == VERTICAL){

            int cmp = Point2D.X_ORDER.compare(query, x.point);

            if      (cmp < 0){
                nearestNeighborSoFar = nearest(x.lb,  query);
            }
            else if (cmp > 0){
                nearestNeighborSoFar =  nearest(x.rt, query);
            }

        }


        return nearestNeighborSoFar;
    }







    // level order traversal
    public Iterable<Node> levelOrder() {

        Queue<Node> nodes = new Queue<Node>();
        Queue<Node> queue = new Queue<Node>();

        queue.enqueue(root);

        while (!queue.isEmpty()) {
            Node x = queue.dequeue();
            if (x == null) continue;
            nodes.enqueue(x);
            queue.enqueue(x.lb);
            queue.enqueue(x.rt);
        }
        return nodes;
    }


    public void draw() {


        for (Node node : this.levelOrder()) {

            draw(node);

         }
    }





    private void draw(Node node) {


        if(node == root){

            rect = new RectHV(0,0,1,1);
            rect.draw();

            double xmin = node.point.x();
            double ymin = node.rect.ymin();
            double xmax = node.point.x();
            double ymax = node.rect.ymax();

            StdDraw.setPenRadius(0.01);
            StdDraw.point(node.point.x(),node.point.y());

            StdDraw.setPenColor(Color.RED);
            StdDraw.setPenRadius();
            StdDraw.line(xmin,ymin,xmax, ymax);




        }

        else if ( node.orientation == HORIZONTAL){

            StdDraw.setPenColor();
            StdDraw.setPenRadius(0.01);
            StdDraw.point(node.point.x(),node.point.y());

            double xmin = node.rect.xmin();
            double ymin = node.point.y();
            double xmax = node.rect.xmax();
            double ymax = node.point.y();

            StdDraw.setPenColor(Color.BLUE);
            StdDraw.setPenRadius();
            StdDraw.line(xmin,ymin,xmax, ymax);

         }

        else if ( node.orientation == VERTICAL){

            StdDraw.setPenColor();
            StdDraw.setPenRadius(0.01);
            StdDraw.point(node.point.x(),node.point.y());

            double xmin = node.point.x();
            double ymin = node.rect.ymin();
            double xmax = node.point.x();
            double ymax = node.rect.ymax();

            StdDraw.setPenColor(Color.RED);
            StdDraw.setPenRadius();
            StdDraw.line(xmin,ymin,xmax, ymax);


        }

    }


    /*

   Range search. To find all points contained in a given query rectangle,
   start at the root and recursively search for points in both subtrees using the following pruning rule:
   if the query rectangle does not intersect the rectangle corresponding to a node,
   there is no need to explore that node (or its subtrees).
   A subtree is searched only if it might contain a point contained in the query rectangle.

    */


    public Iterable<Point2D> range(RectHV rect) {

        plist = new ArrayList<Point2D>();


        if(rect.contains(root.point)){

           plist.add(root.point);

       }

       plist = (ArrayList<Point2D>) range(root.lb, rect, plist);
       plist = (ArrayList<Point2D>) range(root.rt,rect, plist);

       return plist;


    }


    private Iterable<Point2D> range(Node x, RectHV queryRect, ArrayList<Point2D> plist) {

      if(queryRect.intersects(x.rect)){

          if(queryRect.contains(x.point)){

              plist.add(x.point);

          }
          if(x.lb!=null)
          plist = (ArrayList<Point2D>) range(x.lb,queryRect, plist);

          if(x.rt!=null)
          plist = (ArrayList<Point2D>) range(x.rt, queryRect, plist);

      }

     return plist;

    }


    public static void main(String[] args) {
     //   StdDraw.show(0);
        KdTree kdtree = new KdTree();
      //  while (true) {



//        String lineWithSpaces;
//        String[] line;
//        int N = 0;

//        In file = new In("/home/sthangalapally/workspace/Courses/Analysis-I/Assignments/ideaProjects/eralgosone/week5/data/circle10k.txt");
//
//        //   In file = new In("/home/sthangalapally/workspace/Courses/Analysis-I/Assignments/ideaProjects/eralgosone/week5/data/input1M.txt");
//
//        while(!file.isEmpty())
//        {
//            lineWithSpaces = file.readLine().trim();
//            line = lineWithSpaces.split("\\s+");
//
//
//
//            Double i = Double.parseDouble(line[0]);
//            Double j = Double.parseDouble(line[1]);
//
//            Point2D p = new Point2D(i,j);
//            kdtree.insert(p);
//
//        }

                double x0 = 0.7;
                double y0 = 0.2;

                System.out.printf("%8.6f %8.6f\n", x0, y0);
                Point2D p = new Point2D(x0, y0);
                kdtree.insert(p);


                double x1 = 0.5;
                double  y1 = 0.4;

                p = new Point2D(x1, y1);
                kdtree.insert(p);

                double x2 = 0.9;
                double  y2 = 0.6;

                p = new Point2D(x2, y2);
                kdtree.insert(p);

                double x3 = 0.2;
                double y3 = 0.3;

                p = new Point2D(x3,y3);
                kdtree.insert(p);

                double x4 = 0.4;
                double y4 = 0.7;

                p = new Point2D(x4,y4);
                kdtree.insert(p);

                StdDraw.clear();

          kdtree.draw();
        StdDraw.show(50);

//        Point2D p3 = new Point2D(0.13893, 0.795492);
//        System.out.println(kdtree.contains(p3));




//        RectHV rect = new RectHV(0.1, 0.1, 0.97, 0.8);
//
//        //            // draw the rectangle
//            StdDraw.setPenColor(StdDraw.MAGENTA);
//            StdDraw.setPenRadius();
//            rect.draw();
//
//            StdDraw.show(50);
//
//        //            // draw the range search results for kd-tree in blue
//            StdDraw.setPenRadius(.02);
//            StdDraw.setPenColor(StdDraw.BLUE);
//            for (Point2D pn : kdtree.range(rect))
//                pn.draw();
//
//            StdDraw.show(40);


        double x = 0.6;
        double y = 0.1;
        Point2D query = new Point2D(x, y);

        // draw in blue the nearest neighbor (using kd-tree algorithm)
        StdDraw.setPenRadius(.02);
        StdDraw.setPenColor(StdDraw.MAGENTA);
        kdtree.nearest(query).draw();

   //     StdDraw.show(0);
        StdDraw.show(40);




    }
}




