/*************************************************************************
 *  Compilation:  javac KdTreeVisualizer.java
 *  Execution:    java KdTreeVisualizer
 *  Dependencies: StdDraw.java Point2D.java KdTree.java
 *
 *  Add the points that the user clicks in the standard draw window
 *  to a kd-tree and draw the resulting kd-tree.
 *
 *************************************************************************/

public class KdTreeVisualizer {

    public static void main(String[] args) {
        StdDraw.show(0);
        KdTree kdtree = new KdTree();

        String lineWithSpaces;
        String[] line;
        int N = 0;

        In file = new In("/home/sthangalapally/workspace/Courses/Analysis-I/Assignments/ideaProjects/eralgosone/week5/data/kdtree/circle4.txt");

        //   In file = new In("/home/sthangalapally/workspace/Courses/Analysis-I/Assignments/ideaProjects/eralgosone/week5/data/input1M.txt");

        while(!file.isEmpty())
        {
            lineWithSpaces = file.readLine().trim();
            line = lineWithSpaces.split("\\s+");



            Double i = Double.parseDouble(line[0]);
            Double j = Double.parseDouble(line[1]);

            Point2D p = new Point2D(i,j);
            kdtree.insert(p);
            StdDraw.clear();
            kdtree.draw();

        }

    //    kdtree.draw();
        StdDraw.show(50);

//
//        while (true) {
//            if (StdDraw.mousePressed()) {
//                double x = StdDraw.mouseX();
//                double y = StdDraw.mouseY();
//                System.out.printf("%8.6f %8.6f\n", x, y);
//                Point2D p = new Point2D(x, y);
//                kdtree.insert(p);
//                StdDraw.clear();
//                kdtree.draw();
//            }
    //        StdDraw.show(50);
        }

    }

